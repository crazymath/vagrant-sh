 #!/usr/bin/env bash

if [ -e "/etc/vagrant-provisioned" ];
then
    echo "Vagrant provisioning already completed. Skipping..."
    exit 0
else
    echo "Starting Vagrant provisioning process..."
fi

# Install core components
/vagrant/sh/scripts/core.sh

# Install python
/vagrant/sh/scripts/python.sh

# Install postgresql
/vagrant/sh/scripts/postgresql.sh

touch /etc/vagrant-provisioned

echo "--------------------------------------------------"
echo "Your vagrant instance is running"
